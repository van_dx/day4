import scala.io.StdIn
import scala.io.StdIn.readLine
import scala.util.{Try, Success, Failure}

object Main {
  def main(args: Array[String]): Unit = {

  }

  // Bai 1
  val arr = Array(1, 2, 3, 4, 5, 6, 7, 8, 9)

  val res = arr.filter(_ % 2 == 0).slice(1, 3)

  res.foreach(println)
  //  Bai 2
  val x1 = List(1,2,3,4,5,6)

  val x2 = List(2,3,4,9)

  val x3 = x1.diff(x2):::x2.diff(x1)
  println(x3)
}
